/*
 * Copyright 2019 Marco Ehrentreich <marco@ehrentreich.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import grails.util.BuildSettings
import grails.util.Environment
import org.springframework.boot.logging.logback.ColorConverter
import org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter

import java.nio.charset.StandardCharsets

conversionRule 'clr', ColorConverter
conversionRule 'wex', WhitespaceThrowableProxyConverter

// See http://logback.qos.ch/manual/groovy.html for details on configuration
appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        charset = StandardCharsets.UTF_8

        pattern = '%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} ' +
                '%clr(%5p) ' +
                '%clr(---){faint} %clr([%15.15t]){faint} ' +
                '%clr(%-40.40logger{39}){cyan} %clr(:){faint} ' +
                '%m%n%wex'
    }
}

def targetDir = BuildSettings.TARGET_DIR

if (Environment.isDevelopmentMode() && targetDir != null) {
    appender('FULL_STACKTRACE', FileAppender) {
        file = "$targetDir/stacktrace.log"
        append = true

        encoder(PatternLayoutEncoder) {
            charset = StandardCharsets.UTF_8
            pattern = '%level %logger - %msg%n'
        }
    }

    logger('StackTrace', ERROR, ['FULL_STACKTRACE'], false)

    root(INFO, ['STDOUT'])
} else {
    root(WARN, ['STDOUT'])
}
