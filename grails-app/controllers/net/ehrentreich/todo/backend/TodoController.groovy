/*
 * Copyright 2019 Marco Ehrentreich <marco@ehrentreich.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ehrentreich.todo.backend

class TodoController {
    TodosService todosService

    def deleteAll() {
        todosService.deleteAll()
        render view: '/todo/index', model: [todos: Todo.list()]
    }

    def delete() {
        def id = UUID.fromString(params.id as String)
        todosService.delete(id)
        render view: '/todo/index', model: [todos: Todo.list()]
    }

    def index() {
        render view: '/todo/index', model: [todos: Todo.list()]
    }

    def show() {
        def id = UUID.fromString(params.id as String)
        def todo = Todo.get(id)
        render view: '/todo/show', model: [todo: todo]
    }

    def patch() {
        def id = UUID.fromString(params.id as String)
        def patched = todosService.patch(id, request.JSON as Map)
        render view: '/todo/show', model: [todo: patched]
    }

    def save() {
        def urlPrefix = grailsLinkGenerator.link(uri: controllerUri, absolute: true)
        def todoParams = (request.JSON as Map) << [urlPrefix: urlPrefix]
        def todo = todosService.create(todoParams)
        render view: '/todo/show', model: [todo: todo]
    }
}
