package net.ehrentreich.todo.backend

import grails.gorm.transactions.Transactional
import org.codehaus.groovy.runtime.InvokerHelper

@Transactional
class TodosService {
    void deleteAll() {
        Todo.deleteAll(Todo.list())
    }

    void delete(UUID id) {
        def todo = Todo.get(id)
        todo.delete()
    }

    Todo patch(UUID id, Map params) {
        def todo = Todo.get(id)
        InvokerHelper.setProperties(todo, params)
        todo.save()
    }

    Todo create(Map requestParams) {
        def id = UUID.randomUUID()
        def url = "$requestParams.urlPrefix/$id"
        def params = requestParams << [id: id] << [url: url]
        new Todo(params).save()
    }
}
