#!/bin/sh

java -Djava.security.egd=file:/dev/./urandom -Xms512m -Xmx2048m -Dfile.encoding=UTF-8 -jar /app/application.jar --spring.config.additional-location=/data/application.properties
